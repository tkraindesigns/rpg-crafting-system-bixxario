﻿using UnityEngine;

namespace GameDevTV.Inventories
{
    public static class InventoryExtensions
    {
        /// <summary>
        /// Extension method.  Finds and removes given number of item from this inventory.
        /// </summary>
        /// <param name="item">InventoryItem to remove</param>
        /// <param name="number">How many should be removed. Must be non zero (this method will do nothing)</param>
        /// <returns>Number of items remaining.</returns>
        // ReSharper disable once InvalidXmlDocComment
        public static int RemoveItem(this Inventory inventory, InventoryItem item, int number)
        {
            if (item == null) return number;
            while (number > 0)
            {
                int slot = inventory.FindFirstSlot(item);
                if (slot < 0) break;
                int amountRemoved = Mathf.Min(number, inventory.GetNumberInSlot(slot));
                number -= amountRemoved;
                inventory.RemoveFromSlot(slot, amountRemoved);
            }

            return number;
        }
        /// <summary>
        /// Extension Method.
        /// Locates the first inventory slot with the given item.
        /// </summary>
        /// <param name="item">Inventory item to locate.</param>
        /// <returns>Slot containing item, or -1 if not found</returns>
        // ReSharper disable once InvalidXmlDocComment
        public static int FindFirstSlot(this Inventory inventory, InventoryItem item)
        {
            for (int i = 0; i < inventory.GetSize(); i++)
            {
                if (inventory.GetItemInSlot(i) == item)
                {
                    return i;
                }
            }

            return -1;
        }
    }
}